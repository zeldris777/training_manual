# Лабораторная работа №1

Название: `Интегрированные среды разработки Visual Studio Code и CLion`.

Алиас: `hello-world`.

## Задание

В рамках курса изучаются языки программирования:

- C для потока РТ5;
- C++ для потока ИУ5.

Поэтому в учебном материале присутствует информация о C и C++. Студенту необходимо обращать внимание на тот язык программирования, который изучается в рамках потока.

Данная лабораторная работа является вводной. В ней предстоит сделать:

- Изучить материал по установке и настройке средств разработки.
- В зависимости от предпочтений и возможностей, установить Visual Studio Code, CLion или Visual Studio. Для CLion необходимо получить электронный адрес `@student.bmstu.ru`. Если пока нет электронного адреса, то можно воспользоваться "Free 30-day Trial". CLion и Visual Studio являются предпочтительными вариантами.
- Согласно указаниям преподавателя, получить репозиторий в GitLab для выполнения лабораторных работ.
- Выполнить задание в GitLab:
  - Создать новую ветвь от ветви по умолчанию в полученном репозитории.
  - Склонировать репозиторий локально и перейти в созданную ветвь.
  - Открыть папку `lab1` в установленной среде разработки.
  - Раскомментировать код `lab1\main.c` или `lab1\main.cpp`.
  - Собрать и запустить программу.
  - Убедиться в корректной работе программы.
  - Зафиксировать локальные изменения и отправить их в удаленный репозиторий (GitLab).
  - Создать запрос на слияние созданной ветви в ветвь по умолчанию.
  - Убедиться в прохождении теста.
  - Сообщить о выполнении преподавателю.

На момент подготовки данного текста пропущен раздел с описанием работы в Git (будет добавлен значительно позже). О том, как работать с Git см. в дополнительном материале.

В качестве Git GUI рекомендуется использовать [GitKraken](https://www.gitkraken.com/).

## Методический материал

1. [Разработка в операционных систем в Windows 10 и Windows 11](DevelopmentInWindows.md)
2. [Разработка в семействе операционных систем в Linux](DevelopmentInLinux.md)
3. [Разработка в операционной системе macOS](DevelopmentInMacOS.md)
4. [Полезная информация о разработке программы лабораторной работы №1 на C](Programming/C/README.md)
5. [Полезная информация о разработке программы лабораторной работы №1 на C++](Programming/Cpp/README.md)
6. [Дополнительный материал](AdditionalMaterial.md)
