# Дополнительный материал

## Оформление кода

1. [Руководство Google по стилю в C++](https://habr.com/ru/post/480422/) [(eng)](https://google.github.io/styleguide/cppguide.html)
2. [Гайд по оформлению кода на С++ от Стэнфордского университета](https://tproger.ru/translations/stanford-cpp-style-guide/)
3. [How to write Clean, Beautiful and Effective C++ Code](https://clightning.medium.com/how-to-write-clean-beautiful-and-effective-c-code-d4699f5e3864)

## О C

1. [Видеокурс для изучения языка Си с нуля](https://tproger.ru/video/clang-for-beginners-videos/)
2. И. С. Солдатенко. Основы программирования на языке Си
3. [Введение и справочное руководство по языку GNU C](https://w96k.dev/public/doc/gnu-c/)

## О С++

1. Орленко П. А., Евдокимов П. В. С++ на примерах. Практика, практика и только практика

## О Git

1. Фишерман Л. В. Практическое руководство. Управление и контроль версий в разработке программного обеспечения
2. [Введение в Git: от установки до основных команд](https://tproger.ru/translations/beginner-git-cheatsheet/)
3. [Git за полчаса: руководство для начинающих](https://proglib.io/p/git-for-half-an-hour)
4. [Что такое GIT простым языком? Как работает, основные команды GIT](https://www.youtube.com/watch?v=buygCuSqBsA)
